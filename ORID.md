**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview: 后端缺少异常测试，存在console.log，存在UI异常，DoneList刷新页面后会消失
2. Simulation project introduction：老师给我们介绍了team roles，敏捷活动，以及下周时间安排 ，我们也选择了我们的队名。
3. 模拟项目介绍：老师介绍了一下下周要模拟的项目，东南亚电影院订票系统，给了我们一个任务，让我们发散思维去想我们有什么创新，我们中午也一直在讨论这个订票系统，最后我们讨论出来一个团购电影票系统
4. Elevator speech：在电梯演讲中，我们介绍了我们中午准备的电梯演讲，但我们是从用户的角度思考，老师指出要在PO角度思考这个产品的价值
5. User journey：在画user journey时我们讨论的不全面，只聚焦到团购这个功能点上，但没有提出一个比较直观的solution
6. user Story：在user Story时因为我们user journey就没画好，因此我们的user story还不够完善
7. 看板：老师介绍了 trello项目管理工具，简单介绍了工具的使用
8. CI/CD：使用gitlab与railway搭建开发流水线
9. repo：回顾了我们组这周做的不足与优点，提出了几个action，我负责在每次展示之前要监督组员的脱稿能力

**R (Reflective): Please use one word to express your feelings about today's class.**

时间紧，任务重

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

比较粗略的了解了一个敏捷项目开发的流程，为下周的模拟项目做准备

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

1. 提高代码质量，多学习后端技术，小组讨论要更加积极以及注意其他组员的发言
2. 这周我们要把我们没做好的user journey与user story画好并给老师检查